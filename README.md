
# Virtual Machine File Transfer (Beta)

This shell script provides a simple way to copy files from/to a virtual machine disk image.

## Dependencies

`libguestfs`
